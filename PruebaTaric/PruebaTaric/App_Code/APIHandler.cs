﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PruebaTaric.App_Code
{
    public static class APIHandler
    {
        static public MSInfo requestAllInfo(String imdbID, String plot)
        {
            MSInfo msi;
            //URL + ID + APIKey
            String sURL = "http://www.omdbapi.com/?";
            String sID = "i=" + imdbID;
            String sAPIKey = "&apikey=146651e8";
            String sPlotLength = "&plot="+plot;

            String sFinalURL = sURL + sID + sAPIKey + sPlotLength;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@sFinalURL);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                String json = reader.ReadToEnd();
                try
                {
                    msi = JsonConvert.DeserializeObject<MSInfo>(json);
                }//try
                catch
                {
                    //Improve solution
                    //Problem: Getting different format of the JSON
                    msi = new MSInfo();
                    msi.Response = "";
                }//catch
            }//using
            return msi;
        }//requestAllInfo
        static public MainSearch requestSearch(String search, String type)
        {
            MainSearch ms;
            //URL + Search + APIKey
            String sURL = "http://www.omdbapi.com/?";
            String sSearch = "s=" + prepareSearch(search);
            String sType = "&type="+type;
            String sAPIKey = "&apikey=146651e8";

            String sFinalURL = sURL + sSearch + sAPIKey+ sType;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@sFinalURL);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                String json = reader.ReadToEnd();
                try
                {
                    ms = JsonConvert.DeserializeObject<MainSearch>(json);
                }//try
                catch
                {
                    //Improve solution
                    //Problem: Getting different format of the JSON
                    ms = new MainSearch();
                    ms.Response = "";
                }//catch
            }//using
            return ms;
        }//requestSearch
        static public String prepareSearch(String search)
        {
            //WebUtility.UrlEncode doesn't covert:  "-" "_" "." "!" "*" "(" ")"
            string newSearch = WebUtility.UrlEncode(search);
            return newSearch;
        }//prepareSearch
        static public List<Search> getAllResults(String search, String type, double totalResults)
        {
            int iPages = (int) Math.Ceiling(totalResults / 10);
            List<Search> allResults =  new List<Search>();
            for (int i=1; i<=iPages; i++)
            {
                MainSearch ms;
                //URL + Search + APIKey
                String sURL   = "http://www.omdbapi.com/?";
                String sSearch = "s=" + prepareSearch(search);
                String sType   = "&type=" + type;
                String sAPIKey = "&apikey=146651e8";
                String sPage   = "&page=" + i;

                String sFinalURL = sURL + sSearch + sAPIKey + sType+sPage;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@sFinalURL);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    String json = reader.ReadToEnd();
                    ms = JsonConvert.DeserializeObject<MainSearch>(json);
                }//using
                foreach (Search s in ms.Search)
                {
                    allResults.Add(s);
                }//foreach
            }//for
            return allResults;
        }//getAllResults
    }//Class
}//namespace