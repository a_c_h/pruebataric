﻿using Newtonsoft.Json;
using PruebaTaric.App_Code;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PruebaTaric
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            disableToolTips();
        }//MainWindow
        //BackgroundWorker implementation
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            myProgressBar.Value = e.ProgressPercentage;
            lbProgressInfo.Content = e.UserState;
        }//worker_ProgressChanged
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            String[] args = ((String)e.Argument).Split('|');
            //API Request 
            int iPages = (int)Math.Ceiling(double.Parse(args[2]) / 10);
            double progressUnit = 100 / double.Parse(args[2]);
            double progress = 0;
            List<Search> allResults = new List<Search>();
            for (int i = 1; i <= iPages; i++)
            {
                MainSearch ms;
                //URL + Search + APIKey
                String sURL = "http://www.omdbapi.com/?";
                String sSearch = "s=" + APIHandler.prepareSearch(args[0]);
                String sType = "&type=" + args[1];
                String sAPIKey = "&apikey=146651e8";
                String sPage = "&page=" + i;

                String sFinalURL = sURL + sSearch + sAPIKey + sType + sPage;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@sFinalURL);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    String json = reader.ReadToEnd();
                    ms = JsonConvert.DeserializeObject<MainSearch>(json);
                }//using
                foreach (Search s in ms.Search)
                {
                    allResults.Add(s);
                    progress += progressUnit;
                    worker.ReportProgress((int)progress, $"{(int)progress}% - Realizando busqueda en la API");
                }//foreach
            }//for

            //Load results in the listbox
            progressUnit = 100 / (double)allResults.Count;
            progress = 0;
            worker.ReportProgress((int)progress, "Busqueda de la API completada - Cargando información");
            foreach (Search r in allResults)
            {
                //Invoke needed - BackgroundWorker doesn't allow direct UI changes
                _ = this.lbxFound.Dispatcher.Invoke(DispatcherPriority.Normal,
                new Action(() => {
                    lbxFound.Items.Add(r);
                }));
                progress += progressUnit;
                worker.ReportProgress((int)progress, $"{(int)progress}% - Cargando información");
            }//foreach
            worker.ReportProgress(100, "Esperando ordenes :D");
        }//worker_DoWork
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            myProgressBar.Value = 0;
            btSearch.IsEnabled = true;
            cbPlotOption.IsEnabled = true;
            cbTypeOption.IsEnabled = true;
        }//worker_RunWorkerCompleted

        private void BtSearch_Click(object sender, RoutedEventArgs e)
        {
            btSearch.IsEnabled = false;
            cbPlotOption.IsEnabled = false;
            cbTypeOption.IsEnabled = false;
            cleanAll();
            disableToolTips();
            String sTitle = tbTitleOption.Text.ToString();
            String sType = getTypeOption();
            if (!String.IsNullOrEmpty(sTitle) && !String.IsNullOrEmpty(sType))
            {
                MainSearch ms = APIHandler.requestSearch(sTitle, sType);
                
                if (ms.Response.Equals("True"))
                {
                    int iTotalResults = int.Parse(ms.totalResults);
                    lbResults.Content = iTotalResults + " resultados";
                    List<Search> results;
                    if (iTotalResults <= 10) {
                        results = ms.Search;
                    }//if
                    String arg = sTitle + "|" + sType + "|" + iTotalResults;
                    //New implementation
                    //BackgroundWorker implementation
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.RunWorkerCompleted += worker_RunWorkerCompleted;
                    worker.WorkerReportsProgress = true;
                    worker.DoWork += worker_DoWork;
                    worker.ProgressChanged += worker_ProgressChanged;
                    worker.RunWorkerAsync(arg);
                }//if
            }//if
        }//btSearch_Click
        private String getTypeOption()
        {
            int iPosition = cbTypeOption.SelectedIndex;
            switch (iPosition)
            {
                case 0:
                    return "movie";
                case 1:
                    return "series";
                /*case 2:
                    return "episode";*/
            }
            return null;
        }//getTypeOption()
        /*private void loadResults(List<Search> records)
        {
            foreach (Search r in records)
            {
                lbxFound.Items.Add(r);
            }//foreach
        }//loadResults*/

        private void LbxFound_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbAwards.ToolTip.ToString()))
            {
                enableToolTips();
            }
            enableToolTips();
            if (lbxFound.SelectedItem != null)
            {
                Search r = (Search)lbxFound.SelectedItem;
                String sPlotLength = getPlotOption();
                MSInfo msi = APIHandler.requestAllInfo(r.imdbID, sPlotLength);
                if (msi.Response.Equals("True"))
                {
                    //Adding img
                    var bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    try
                    {
                        bitmapImage.UriSource = new Uri(msi.Poster);
                    }
                    catch//If there are some problems with the Uri
                    {
                        bitmapImage.UriSource = new Uri("/Resources/img_not_found.png", UriKind.Relative);
                    }
                    bitmapImage.EndInit();
                    imgPoster.Source = bitmapImage;
                    //}
                    //Other components
                    tbTitle.Text = msi.Title;
                    tbRuntime.Text = msi.Runtime;
                    tbRelease.Text = msi.Released;
                    tbGenre.Text = msi.Genre;
                    tbDirector.Text = msi.Director;
                    //Writer/s
                    loadWriters(msi);
                    loadActors(msi);
                    tbLanguage.Text = msi.Language;
                    tbCountry.Text = msi.Country;
                    tbAwards.Text = msi.Awards;
                    tbPlot.Text = msi.Plot;
                    tbProduction.Text = msi.Production;
                    tbBoxOffice.Text = msi.BoxOffice;
                    loadRatings(msi);
                    tbimdbRating.Text = msi.imdbRating;
                    tbimdbVotes.Text = msi.imdbVotes;
                    tbMetascore.Text = msi.Metascore;
                    tbWebsite.Text = msi.Website;
                }//if
            }//if
        }//LbxFound_SelectionChanged
        private String getPlotOption()
        {
            int iPosition = cbPlotOption.SelectedIndex;
            switch (iPosition)
            {
                case 0:
                    return "short";
                case 1:
                    return "full";
            }//switch
            return "short";
        }//getPlotOption

        private void CbPlotOption_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //TOFIX: Improve this try-catch, try to find other solution
            try {
                Search r = (Search)lbxFound.SelectedItem;
                String sPlotLength = getPlotOption();
                MSInfo msi = APIHandler.requestAllInfo(r.imdbID, sPlotLength);
                if (msi.Response.Equals("True"))
                {
                    tbPlot.Text = msi.Plot;
                }//if
            }//try
            catch { }
        }//cbPlotOption_SelectionChanged
        private void cleanAll()
        {
            //Cleaning img
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource = new Uri("/Resources/img_not_found.png", UriKind.Relative);
            bitmapImage.EndInit();
            imgPoster.Source = bitmapImage;
            lbxFound.Items.Clear();
            lbResults.Content = "0 resultados";
            //Other components
            tbTitle.Text = "";
            tbRuntime.Text = "";
            tbRelease.Text = "";
            tbGenre.Text = "";
            tbDirector.Text = "";
            spWriter.Children.Clear();
            spActors.Children.Clear();
            tbLanguage.Text = "";
            tbCountry.Text = "";
            tbAwards.Text = "";
            tbPlot.Text = "";
            tbProduction.Text = "";
            tbBoxOffice.Text = "";
            spRatings.Children.Clear();
            tbimdbRating.Text = "";
            tbimdbVotes.Text = "";
            tbMetascore.Text = "";
            tbWebsite.Text = "";
        }//cleanAll
        private void enableToolTips()
        {
            ToolTipService.SetIsEnabled(tbTitle, true);
            ToolTipService.SetIsEnabled(tbRuntime, true);
            ToolTipService.SetIsEnabled(tbRelease, true);
            ToolTipService.SetIsEnabled(tbGenre, true);
            ToolTipService.SetIsEnabled(tbDirector, true);
            ToolTipService.SetIsEnabled(tbLanguage, true);
            ToolTipService.SetIsEnabled(tbCountry, true);
            ToolTipService.SetIsEnabled(tbAwards, true);
            ToolTipService.SetIsEnabled(tbPlot, true);
            ToolTipService.SetIsEnabled(tbProduction, true);
            ToolTipService.SetIsEnabled(tbBoxOffice, true);
            ToolTipService.SetIsEnabled(tbimdbRating, true);
            ToolTipService.SetIsEnabled(tbimdbVotes, true);
            ToolTipService.SetIsEnabled(tbMetascore, true);
            ToolTipService.SetIsEnabled(tbWebsite, true);
        }//enableToolTips
        private void disableToolTips()
        {
            ToolTipService.SetIsEnabled(tbTitle, false);
            ToolTipService.SetIsEnabled(tbRuntime, false);
            ToolTipService.SetIsEnabled(tbRelease, false);
            ToolTipService.SetIsEnabled(tbGenre, false);
            ToolTipService.SetIsEnabled(tbDirector, false);
            ToolTipService.SetIsEnabled(tbLanguage, false);
            ToolTipService.SetIsEnabled(tbCountry, false);
            ToolTipService.SetIsEnabled(tbAwards, false);
            ToolTipService.SetIsEnabled(tbPlot, false);
            ToolTipService.SetIsEnabled(tbProduction, false);
            ToolTipService.SetIsEnabled(tbBoxOffice, false);
            ToolTipService.SetIsEnabled(tbimdbRating, false);
            ToolTipService.SetIsEnabled(tbimdbVotes, false);
            ToolTipService.SetIsEnabled(tbMetascore, false);
            ToolTipService.SetIsEnabled(tbWebsite, false);
        }//disableToolTips
        private void loadWriters(MSInfo msi)
        {
            spWriter.Children.Clear();
            String sWriters = msi.Writer;
            if (sWriters.IndexOf(',') == -1)
            {
                TextBlock tb = new TextBlock();
                tb.Text = sWriters;
                spWriter.Children.Add(tb);
            }//if
            else
            {
                string[] asWriters = sWriters.Split(',');
                foreach(string s in asWriters)
                {
                    TextBlock tb = new TextBlock();
                    tb.Text = s.Trim();
                    spWriter.Children.Add(tb);
                }//foreach
            }//else
        }//loadWriters
        private void loadActors(MSInfo msi)
        {
            spActors.Children.Clear();
            String sActors = msi.Actors;
            if (sActors.IndexOf(',') == -1)
            {
                TextBlock tb = new TextBlock();
                tb.Text = sActors;
                spActors.Children.Add(tb);
            }//if
            else
            {
                string[] asActors = sActors.Split(',');
                foreach (string s in asActors)
                {
                    TextBlock tb = new TextBlock();
                    tb.Text = s.Trim();
                    spActors.Children.Add(tb);
                }//foreach
            }//else
        }//loadActors
        private void loadRatings(MSInfo msi)
        {
            spRatings.Children.Clear();
            List<Rating> ratings = msi.Ratings;
            if (ratings != null)
            {
                foreach(Rating r in ratings)
                {
                    String sRating = "";
                    sRating += r.Source + " - ";
                    sRating += r.Value;
                    TextBlock tb = new TextBlock();
                    tb.Text = sRating;
                    spRatings.Children.Add(tb);
                }//foreach
            }//if
        }//loadRatings
    }//Class
}//namespace