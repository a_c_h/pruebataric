﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PruebaTaric.App_Code;

namespace PruebaTaricTests
{
    [TestClass]
    public class APIHandlerTests
    {
        //static public MSInfo requestAllInfo(String imdbID, String plot)
        //-- Mocking needed
        //-- API

        //static public MainSearch requestSearch(String search, String type)
        //-- Mocking needed
        //-- API

        //static public List<Search> getAllResults(String search, String type, double totalResults)
        //-- Mocking needed
        //-- API

        //static public String prepareSearch(String search)
        /*[TestMethod]
        public void ampersand_Return_percentage26()
        {
            //Arrange
            var sSearch = "&";
            String expectedResponse = "%26";
            //Act
            var result = APIHandler.prepareSearch(sSearch);
            //Assert
            Assert.AreEqual(expectedResponse, result);
        }//ampersand_Return_percentage26*/
        [TestMethod]
        public void criticalChars_Return_encodedCharacters()
        {
            //Arrange
            String[] criticalChars = new[] { " ", "\"", "#", "%", "&", "'", "+", ",", ":"};
            String[] expectedResponse = new[] { "+", "%22", "%23", "%25", "%26", "%27", "%2B", "%2C", "%3A" };
            //Act
            for(int i=0; i<criticalChars.Length; i++)
            {
                //Act
                var result = APIHandler.prepareSearch(criticalChars[i]);
                //Assert
                Assert.AreEqual(expectedResponse[i], result);
            }//foreach
        }//criticalCharacters_Return_encodedCharacters
        [TestMethod]
        public void stringSpecialChar_Return_stringCharEncoded()
        {
            //Arrange
            var sSearch = "Fast #";
            String expectedResponse = "Fast+%23";
            //Act
            var result = APIHandler.prepareSearch(sSearch);
            //Assert
            Assert.AreEqual(expectedResponse, result);
        }//stringAmpersand_Return_stringPercentage26
        [TestMethod]
        public void stringSpecialCharstring_Return_stringCharEncodedstring()
        {
            //Arrange
            var sSearch = "Fast & Furious";
            String expectedResponse = "Fast+%26+Furious";
            //Act
            var result = APIHandler.prepareSearch(sSearch);
            //Assert
            Assert.AreEqual(expectedResponse, result);
        }//stringAmpersandstring_Return_stringPercentage26string
        [TestMethod]
        public void null_return_null()
        {
            //Arrange
            String sSearch = null;
            String expectedResponse = null;
            //Act
            var result = APIHandler.prepareSearch(sSearch);
            //Assert
            Assert.AreEqual(expectedResponse, result);
        }//null return null
        [TestMethod]
        public void encoded_return_x()
        {
            //Arrange
            String sSearch = "Fast%20%26%20Furious";
            String expectedResponse = "Fast%20%26%20Furious";
            //Act
            var result = APIHandler.prepareSearch(sSearch);
            //Assert
            Assert.AreEqual(expectedResponse, result);
        }//encoded_return_x
    }//class
}//namespace