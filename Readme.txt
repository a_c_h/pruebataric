Exe:
PruebaTaric\PruebaTaric\bin\Debug

App_Code:
	APIHandler.cs //Consultas a la API
		- Sin propiedades / es static
		- M�todos:
			static public MSInfo requestAllInfo(String imdbID, String plot) //Suprimido - MainWindow
			static public MainSearch requestSearch(String search, String type)
			static public String prepareSearch(String search)
			static public List<Search> getAllResults(String search, String type, double totalResults)
	MainSearch.cs //Manejador del JSON recibido de las busquedas
		- Contiene clases y sus respectivas propiedades con m�todos get y set
		- Clases:
			MainSearch: Busqueda principal que devuelve si hay respuesta, cantidad de resultados,
						y pre-info de cada resultado encontrado.
			Search: Pre-info de cada resultado encontrado.
	MSInfo.cs //Manejador del JSON recibido de cada pel�cula/serie
		- Contiene clases y sus respectivas propiedades con m�todos get y set
		- Clases:
			MSInfo: Busqueda principal que devuelve toda la informaci�n de la pel�cula/serie buscada.
			Rating: Hay una propiedad en MSInfo que es una lista de Ratings, contiene quien ha valorado y
					la valoraci�n que ha hecho.
MainWindow:
	MainWindow.xaml.cs //Gestiona componentes de la pantalla principal
		- M�todos:
			public MainWindow()
			private void BtSearch_Click(object sender, RoutedEventArgs e)
			private String getTypeOption()
			private void loadResults(List<Search> records) //Suprimido
			private void LbxFound_SelectionChanged(object sender, SelectionChangedEventArgs e)
			private String getPlotOption()
			private void CbPlotOption_SelectionChanged(object sender, SelectionChangedEventArgs e)
			private void cleanAll()
			private void enableToolTips()
			private void disableToolTips()
			private void loadWriters(MSInfo msi)
			private void loadActors(MSInfo msi)
			private void loadRatings(MSInfo msi)
		- Extra: Debido a que una vez terminada la primera versi�n me di cuenta de que al hacer una consulta
			 a la API que devolviese muchos resultados (Por ejemplo "Big" devuelve +3000), implica
			 que la interfaz se congele debido a que este proceso se ejecuta en el hilo principal, por ello
			 esta tarea asi como la de poner dichos resultados en un listBox ha sido delegada a la
			 clase BackgroundWorker, que genera un hilo que se encarga de realizar estas tareas y as� la
			 interfaz queda libre y no parece que se ha producido un error o el programa se ha congelado
			 por completo.
UnitTests:
	Actualmente existen pruebas de test unitario. #TODO